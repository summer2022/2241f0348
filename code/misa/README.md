# RISC-V misa
---

RISC-V Specs 演示代码。

## 环境

### linux-lab

```
$ git clone https://gitee.com/tinylab/cloud-lab.git
$ cd cloud-lab/
$ tools/docker/run linux-lab
$ tools/docker/bash
```

### source code

```
$ git clone https://gitee.com/tinylab/riscv-linux
$ cd code/misa/
```

代码运行在 `M-Mode`，使用 `qemu` 测试。

## asm

```
cd asm
make debug
```

`misa.md` 有详细说明。

## c

```
cd c
make run
```

C 语言能查看更多的信息。
