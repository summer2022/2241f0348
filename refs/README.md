
# 参考资料

## 思维导图

我们简单梳理了一下该项目可能涉及的资料，并制作成了思维导图，欢迎下载查阅。

* [riscv-linux.xmind][006]

## 更多信息

该目录下的相关资料、链接等收集自网络，仅供学习和研究使用，版权归原作者所有，使用时请务必遵循相关知识产权。

* 邮件列表
    * <https://lore.kernel.org/linux-riscv/>

* RVOS 视频课程
    * Code: <https://gitee.com/tinylab/rvos-lab>
    * Video: <https://www.bilibili.com/video/BV1Q5411w7z5>

* Linux 移植到新架构
    * Porting Linux to a new processor architecture (kernel itself)
        * [part 1: The basics][022]，[译文][026] @通天塔
        * [part 2: The early code][023], [译文][027] @通天塔
        * [part 3: To the finish line][024], [译文][028] @通天塔
    * [Base porting Linux kernel RISC-V archiecture][005]
    * [Porting Linux to a new architecture (not only kernel)][021], [pdf][004]

* RISC-V 汇编
    * [在 Linux Lab Disk 下开展 RISC-V 汇编语言实验][029]
    * [RISC-V ASSEMBLY LANGUAGE Programmer Manual Part I][025]
    * [Introduction to Assembly: RISC-V Instruction Set Architecture][020]
    * [RISC-V Assembly Programmer's Manual][008]

* RISC-V 标准与规范
    * [RISC-V Platform Specification][018]
    * [RISC-V ABIs Specification][009]
        * [The RISC-V psABI][002]
    * [RISC-V Supervisor Binary Interface Specification][010]
    * [RISC-V UEFI Protocols][012]
    * [RISC-V Instruction Set Manual][015]
        * [Volume I: User-Level ISA][017]
        * [Volume II: Privileged Architecture][016]
    * Extensions
        * [External Debug Support][014]
        * [Trace Specification][011]
        * [RISC-V Cryptography Extension][013]
        * [RISC-V Vector Specification][019]

* 硬件资料
    * [平头哥开放的 RISC-V 核][007]
    * [平头哥 D1 资料][003]

* 在线书籍
    * [The RISC-V Reader: An Open Architecture Atlas][030]
    * [Linux Insides][001]

[001]: https://0xax.gitbooks.io/linux-insides/content/index.html
[002]: https://courses.cs.washington.edu/courses/cse481a/20sp/notes/psabi.pdf
[003]: https://dl.linux-sunxi.org/D1/
[004]: https://elinux.org/images/5/50/Rybczynska_Porting_Linux_to_a_new_architecture_ELC2014.pdf
[005]: https://elinux.org/images/c/c7/Base-porting-linux-kernel-riscv-archiecture-ELC-2019.pdf
[006]: https://gitee.com/tinylab/riscv-linux/raw/master/refs/riscv-linux.xmind
[007]: https://github.com/orgs/T-head-Semi/repositories?type=all
[008]: https://github.com/riscv-non-isa/riscv-asm-manual/blob/master/riscv-asm.md
[009]: https://github.com/riscv-non-isa/riscv-elf-psabi-doc/blob/master/riscv-abi.adoc
[010]: https://github.com/riscv-non-isa/riscv-sbi-doc/blob/master/riscv-sbi.adoc
[011]: https://github.com/riscv-non-isa/riscv-trace-spec/
[012]: https://github.com/riscv-non-isa/riscv-uefi/releases
[013]: https://github.com/riscv/riscv-crypto
[014]: https://github.com/riscv/riscv-debug-spec/
[015]: https://github.com/riscv/riscv-isa-manual
[016]: https://github.com/riscv/riscv-isa-manual/releases/download/Priv-v1.12/riscv-privileged-20211203.pdf
[017]: https://github.com/riscv/riscv-isa-manual/releases/download/Ratified-IMAFDQC/riscv-spec-20191213.pdf
[018]: https://github.com/riscv/riscv-platform-specs/blob/main/riscv-platform-spec.adoc
[019]: https://github.com/riscv/riscv-v-spec/
[020]: https://inst.eecs.berkeley.edu/~cs61c/sp21/pdfs/docs/lectures/lec06_assembly.key.pdf
[021]: https://lwn.net/Articles/597351/
[022]: https://lwn.net/Articles/654783/
[023]: https://lwn.net/Articles/656286/
[024]: https://lwn.net/Articles/657939/
[025]: https://shakti.org.in/docs/risc-v-asm-manual.pdf
[026]: https://tinylab.org/lwn-654783/
[027]: https://tinylab.org/lwn-656286/
[028]: https://tinylab.org/lwn-657939/
[029]: https://zhuanlan.zhihu.com/p/479295461
[030]: http://www.riscvbook.com/chinese/RISC-V-Reader-Chinese-v2p1.pdf
